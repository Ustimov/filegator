# README #

## pre v0.2 ##

Проект VS2013 с настроенным окружением.

Для сборки проекта, нужно добавить две переменные среды окружения:
* BOOSTDIR - указывает на папку с boost;
* WXDIR - указывает на папку с wxWidgets.

Разумеется, предварительно boost и wx надо собрать.

## v0.1 ##

### Компиляция с MinGW ###

* Качаем MinGW и исходники boost.
* Устанавливаем MinGW и добавляем папку bin из директории MinGW в Path.
* Создаём переменную окружения %BOOST_PATH%, указывающую на папку с распакованным boost.
* Компилируем несколько частей boost (вводим в консоли из папки с boost:
bootstrap mingw 
b2 toolset=gcc system
b2 toolset=gcc thread.
* В папке bin.v2 будут скомпилированные бинарные файлы, которые нужно перенести в отдельную папку, на которую указать с помощью переменной окружений %BOOST_LIBS%.

### Компиляция с Visual Studio ###

* Качаем boost и компилируем:
bootstrap msvc
b2 toolset=msvc system
b2 toolset=msvc chrono
b2 toolset=msvc date_time
b2 toolset=msvc regex
b2 toolset=msvc thread.
* Заходим в Properties проекта.
* Указываем в С/C++ > General > Additional Include Directories путь к папке с boost.
* Указываем в Linker > Input > Additional Dependencies путь к lib файлам, требуемым при компиляции:
C:\boost_1_57_0\bin.v2\libs\thread\build\msvc-12.0\debug\link-static\threading-multi\libboost_thread-vc120-mt-gd-1_57.lib
C:\boost_1_57_0\bin.v2\libs\date_time\build\msvc-12.0\debug\link-static\threading-multi\libboost_date_time-vc120-mt-gd-1_57.lib
C:\boost_1_57_0\bin.v2\libs\system\build\msvc-12.0\debug\link-static\threading-multi\libboost_system-vc120-mt-gd-1_57.lib
C:\boost_1_57_0\bin.v2\libs\chrono\build\msvc-12.0\debug\link-static\threading-multi\libboost_chrono-vc120-mt-gd-1_57.lib
C:\boost_1_57_0\bin.v2\libs\regex\build\msvc-12.0\debug\link-static\threading-multi\libboost_regex-vc120-mt-gd-1_57.lib.

* Проклинаем тот день, когда решили связаться с C++.