#include "MainFrame.h"
#include "FilegatorApp.h"

#define APP_NAME "Filegator"

namespace filegator {

	wxIMPLEMENT_APP(FilegatorApp);

	bool FilegatorApp::OnInit() {
		MainFrame * frame = new MainFrame(APP_NAME, wxPoint(50, 50), wxSize(700, 450));
		frame->Show(true);
		return true;
	}
}