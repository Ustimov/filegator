#include "HammingCode.h"

void HammingCode::byteToArray(unsigned char byte, bool *array) {
	for (int i = 0; i < 8; i++) {
		array[i] = byte % 2;
		byte /= 2;
	}
}

void HammingCode::arrayToByte(bool *array, unsigned char& byte) {
	byte = 0;
	for (int i = 0, p = 1; i < 8; i++, p *= 2) {
		byte += array[i] * p;
	}
}

void HammingCode::encode(unsigned char byte, unsigned char& left, unsigned char& right) {
	bool byteArr[8], leftArr[8], rightArr[8];
	byteToArray(byte, byteArr);
	bool *halfArr = rightArr;
	for (int i = 0, shift = 0; i < 2; i++, shift += 4) {
		halfArr[2] = byteArr[0 + shift];
		halfArr[4] = byteArr[1 + shift];
		halfArr[5] = byteArr[2 + shift];
		halfArr[6] = byteArr[3 + shift];
		halfArr[7] = 0;

		halfArr[0] = halfArr[2] ^ halfArr[4] ^ halfArr[6];
		halfArr[1] = halfArr[2] ^ halfArr[5] ^ halfArr[6];
		halfArr[3] = halfArr[4] ^ halfArr[5] ^ halfArr[6];

		halfArr = leftArr;
	}
	arrayToByte(leftArr, left);
	arrayToByte(rightArr, right);
}

bool HammingCode::checkForError(unsigned char& byte) {
	bool byteArr[8];
	byteToArray(byte, byteArr);
	int syndrome = 1 * (byteArr[0] ^ byteArr[2] ^ byteArr[4] ^ byteArr[6]) +
		2 * (byteArr[1] ^ byteArr[2] ^ byteArr[5] ^ byteArr[6]) +
		4 * (byteArr[3] ^ byteArr[4] ^ byteArr[5] ^ byteArr[6]);
	syndrome -= 1;
	if (syndrome > -1)
		return false;
	else 
		return true;
	/*
	if (syndrome > -1)
		byteArr[syndrome] = !byteArr[syndrome];
	arrayToByte(byteArr, byte);
	*/
}

bool HammingCode::decode(unsigned char& byte, unsigned char left, unsigned char right) {
	bool leftOK = HammingCode::checkForError(left);
	bool rightOK = HammingCode::checkForError(right);
	if (!leftOK || !rightOK)
		return false;
	bool byteArr[8], leftArr[8], rightArr[8];
	byteToArray(left, leftArr);
	byteToArray(right, rightArr);
	bool *halfArr = rightArr;
	for (int i = 0, shift = 0; i < 2; i++, shift += 4) {
		byteArr[0 + shift] = halfArr[2];
		byteArr[1 + shift] = halfArr[4];
		byteArr[2 + shift] = halfArr[5];
		byteArr[3 + shift] = halfArr[6];
		halfArr = leftArr;
	}
	arrayToByte(byteArr, byte);
	return true;
}
