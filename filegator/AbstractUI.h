#include <string>

#ifndef ABSTRACTUI_H_
#define ABSTRACTUI_H_

class AbstractUI {
public:
	virtual std::string getSavePath(std::string fileName) = 0;
	virtual void showProgress(std::string message) = 0;
	virtual void updateProgress(int percent) = 0;
	virtual void finishProgress() = 0;
	virtual void updateConnectionStatus(bool connection) = 0;
	virtual void log(std::string message) = 0;
};

#endif // ABSTRACTUI_H_