#include <iostream>
#include "Manager.h"


namespace filegator {
	Manager::Manager(std::string portName, int baudRate, AbstractUI * ui) {
		pauseRun = false;
		stopThreads = false;
		
		frameFactory = NULL;
		fileBuilder = NULL;
		
		this->portName = portName;
		this->baudRate = baudRate;
		this->ui = ui;
		serialPort = new SerialPort(portName, baudRate);

		boost::thread workerRead(&Manager::runThread, this);
		boost::thread workerCheck(&Manager::checkConnection, this);

		buf = new byte[HEADER_LENGTH];
		serialPort->read(buf, HEADER_LENGTH, &Manager::onReceiveHeader, this);
	}

	Manager::~Manager() {
		stopThreads = true;
		if (frameFactory != NULL) {
			delete frameFactory;
		}
		if (fileBuilder != NULL) {
			delete fileBuilder;
		}
		delete serialPort;
		delete[] buf;
	}

	void Manager::onReceiveHeader(const boost::system::error_code & ec, size_t bytes_transferred) {
		if (buf[1] == AbstractFrame::INFO) {
			int len = buf[2]; // �������� ����� ��������������� ����
			memcpy(numberArrayBuf, buf + 3, 2); // ��������� ����� �����
			delete[] buf;
			buf = new byte[len + 1]; // �������������� �������� ��� ��������� �����
			serialPort->read(buf, len + 1, &Manager::onReceiveInfoFrame, this);
		}
		else if (buf[1] == AbstractFrame::ACK) {
			AckFrame ackFrame(buf);
			frameFactory->delivered(ackFrame.getNumber());
			int nextFrameNumber = ackFrame.getNumber() + 1;
			ui->updateProgress(frameFactory->getPercent());
			if (frameFactory->getFrameCount() > nextFrameNumber) {
				InfoFrame * frame = (InfoFrame*)frameFactory->getFrame(nextFrameNumber);
				frame->encodeData();
				sendFrame(frame);
			}
			else {
				// ��������� ���������� ��������
				ui->finishProgress();
				delete frameFactory;
				frameFactory = NULL;
				ui->log("�������� ����� ���������");
			}
			listenPort();
		}
		else if (buf[1] == AbstractFrame::INIT) {
			int len = buf[2];
			delete[] buf;
			buf = new byte[len + 1];
			serialPort->read(buf, len + 1, &Manager::onReceiveInitFrame, this);
		}
		else if (buf[1] == AbstractFrame::RET) {
			RetFrame retFrame(buf);
			ui->log("������ ������ �� ��������� �������� ����� " + std::to_string(retFrame.getNumber()));
			sendFrame(frameFactory->getFrame(retFrame.getNumber()));
			listenPort();
		}
		else {
			// �������� ����������� ���������� ������ ��������� ������
			ui->log("������� �����");
		}
	}

	void Manager::onReceiveInitFrame(const boost::system::error_code& ec, size_t bytes_transferred) {
		InitFrame initFrame(buf, bytes_transferred - 1); // �������� �������, ����� ��������� �������� ����

		fileBuilder = new FileBuilder(initFrame.getFileName(), initFrame.getFileSize());

		AckFrame * ackFrame = new AckFrame(initFrame.getNumber());
		sendFrame(ackFrame);
		delete ackFrame;
		
		ui->log("���������� ��������� ����� " + initFrame.getFileName());
		ui->showProgress("��������� �����");

		listenPort();
	}

	void Manager::onReceiveInfoFrame(const boost::system::error_code & ec, size_t bytes_transferred) {
		InfoFrame * infoFrame = new InfoFrame(buf, bytes_transferred - 1, numberArrayBuf);

		if (infoFrame->getNumber() == fileBuilder->getFrameCount() + 1 && infoFrame->decodeData()) {
			AckFrame * ackFrame = new AckFrame(infoFrame->getNumber());
			sendFrame(ackFrame);
			fileBuilder->add(infoFrame);
			delete ackFrame;
			
			ui->updateProgress(fileBuilder->getPercent());

			if (fileBuilder->getPercent() == 100) {
				ui->finishProgress();
				std::string path = ui->getSavePath(fileBuilder->getFileName());
				fileBuilder->write(path);
				ui->log("���� " + path + " ��������");
				delete fileBuilder;
				fileBuilder = NULL;
			}
			listenPort();
		}
		else {
			// ����������� ���� �� ������ ���������� ����������� + 1
			RetFrame * retFrame = new RetFrame(fileBuilder->getFrameCount() + 1);
			sendFrame(retFrame);
			ui->log("���������� ������, ���������� ��������� �������� ����� "
				+ std::to_string(fileBuilder->getFrameCount() + 1));
			delete retFrame;
			delete infoFrame;
			
			listenPort();
		}
	}

	void Manager::checkConnection() {
		bool connection;
		bool restored = false;
		while (!stopThreads) {
			connection = serialPort->isCD();
			ui->updateConnectionStatus(connection);
			if (!connection && !restored) {
				if (frameFactory != NULL || (fileBuilder != NULL && fileBuilder->getPercent() != 100)) {
					resume();
					restored = true;
				}
			}
			else if (connection) {
				restored = false;
			}
			boost::this_thread::sleep(boost::posix_time::seconds(1));
		}
	}

	void Manager::sendFile(std::string path) {
		ui->log("���������� �������� ����� " + path);
		frameFactory = new FrameFactory(path);
		
		if (frameFactory->getFrameCount() < 2) {
			ui->log("������ ���� ���������� ������");
			delete frameFactory;
			frameFactory = NULL;
			return;
		}
		
		// ��������� ������ ����
		ui->showProgress("�������� �����");
		sendFrame(frameFactory->getFrame(0));
	}

	void Manager::sendFrame(AbstractFrame * frame) {
		int len;
		const byte * data = frame->getSerialized(len);
		serialPort->write(data, len);
	}

	void Manager::resume() {
		pauseRun = true;
		delete serialPort;
		boost::this_thread::sleep(boost::posix_time::seconds(1));
		serialPort = new SerialPort(portName, baudRate);
		pauseRun = false;

		boost::this_thread::sleep(boost::posix_time::seconds(1));

		if (frameFactory != NULL) {
			sendFrame(frameFactory->getFrame(1));
		}
		listenPort();
	}

	void Manager::listenPort() {
		delete[] buf;
		buf = new byte[HEADER_LENGTH];
		serialPort->read(buf, HEADER_LENGTH, &Manager::onReceiveHeader, this);
	}

	void Manager::runThread() {
		while (!stopThreads) {
			if (!pauseRun) {
				serialPort->run();
			}
			else {
				boost::this_thread::sleep(boost::posix_time::seconds(1));
			}
		}
	}
}
