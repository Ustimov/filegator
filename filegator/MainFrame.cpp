#include "MainFrame.h"
#include "FilegatorApp.h"
#include "FrameFactory.h"

namespace filegator {
	wxBEGIN_EVENT_TABLE(MainFrame, wxFrame)
	  //EVT_MENU(ID_OPEN, MainFrame::onOpen)
	  //EVT_MENU(ID_ERROR, MainFrame::onError)
	  EVT_MENU(wxID_EXIT, MainFrame::onExit)
	  EVT_MENU(wxID_ABOUT, MainFrame::onAbout)
	wxEND_EVENT_TABLE()

	MainFrame::MainFrame(const wxString & title, const wxPoint & pos, const wxSize & size)
		: wxFrame(NULL, wxID_ANY, title, pos, size) {

		wxMenu * menu_file = new wxMenu;
		//menu_file->Append(ID_OPEN, wxT("�������"), wxT("������� ���� ��� ��������"));
		//menu_file->AppendSeparator();
		menu_file->Append(wxID_EXIT, wxT("�����"), wxT("�������� ���������"));
		
		/*
		wxMenu * menu_error = new wxMenu;
		menu_error->Append(ID_ERROR, wxT("�������� �������� ������"),
			wxT("�������� �������� ������ � ���� ����� 50% �������� ��� ������������ ����������� ������"));
		*/

		wxMenu * menu_help = new wxMenu;
		menu_help->Append(wxID_ABOUT, wxT("� ���������"), wxT("���������� ���������� � ���������"));

		wxMenuBar * menu_bar = new wxMenuBar;
		menu_bar->Append(menu_file, wxT("����"));
		//menu_bar->Append(menu_error, wxT("������"));
		menu_bar->Append(menu_help, wxT("�������"));
  
		SetMenuBar(menu_bar);
  
		CreateStatusBar();
		SetStatusText(wxT("Filegator v0.5"));

		tabPanel = new TabPanel(this, 10, 10, 300, 100);

		SetIcon(wxICON(AAAA));
	}

	void MainFrame::onExit(wxCommandEvent & event) {
		Close(true);
	}

	void MainFrame::onAbout(wxCommandEvent & event) {
		wxMessageBox(wxT("Filegator\n\n��������� ��� �������� ������ ����� COM-����\
			\n\n�����������:\n������� �.�.\n��������� �.�.\n����� �.�.\
			\n\n���� ��. �������, ������� ��5, 2015 ���\
			\n\nE-mail: art@ustimov.org"),
			wxT("� ���������"), wxOK | wxICON_INFORMATION);
	}

	/*
	void MainFrame::onOpen(wxCommandEvent & event) {
		wxLogMessage("���� ������� (��������)");
		
		//wxFileDialog * openFileDialog = new wxFileDialog(this);
		//wxString fileName;
		//if (openFileDialog->ShowModal() == wxID_OK) {
		//	fileName = openFileDialog->GetPath();
		//}
		
		//tabPanel->getCurrentPage()->RecordToLog(fileName);
	}
	*/

	/*
	void MainFrame::onError(wxCommandEvent & event) {
		FrameFactory::spoilFlag = true;
		wxLogMessage("�������� ������ ��������");
	}
	*/
}