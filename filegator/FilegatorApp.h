#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#ifndef FILEGATORAPP_H_
#define FILEGATORAPP_H_

namespace filegator {
	class FilegatorApp : public wxApp {
	public:
		virtual bool OnInit();
	};
}

#endif // FILEGATORAPP_H_

