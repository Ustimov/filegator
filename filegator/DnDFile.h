#define WIN32_LEAN_AND_MEAN
#include <wx/wxprec.h>
#include <wx/dnd.h>
#include "ComPanel.h"

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#ifndef DNDFILE_H_
#define DNDFILE_H_

namespace filegator {
	class DnDFile : public wxFileDropTarget {
		wxListBox * dndField;
		ComPanel * comPanel;
		virtual bool OnDropFiles(wxCoord x, wxCoord y, const wxArrayString & filenames);
	public:
		DnDFile(wxListBox * dndField, ComPanel * comPanel);
	};
}

#endif // DNDFILE_H_