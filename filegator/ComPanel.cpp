#include "ComPanel.h"
#include "DnDFile.h"

namespace filegator {
	wxBEGIN_EVENT_TABLE(ComPanel, wxPanel)
		EVT_BUTTON(ID_CONNECT_BUTTON, ComPanel::onConnectButtonClick)
		EVT_BUTTON(ID_DISCONNECT_BUTTON, ComPanel::onDisconnectButtonClick)
	wxEND_EVENT_TABLE()

	ComPanel::ComPanel(wxPanel * panel) : wxPanel(panel) {
		initOptionBoxSizer();
		initSendFileBoxSizer();
		SetSizer(sendFileBoxSizer, false);
		sendFileBoxSizer->Show(false);
		SetSizer(optionBoxSizer, false);
		progressDialog = NULL;
	}
	
	void ComPanel::updateConnectionStatus(bool connection) {
		if (connection) {
			dndFieldListBox->SetBackgroundColour(*wxGREEN);
			dndFieldListBox->Enable(true);
			dndFieldListBox->Refresh();
		}
		else {
			dndFieldListBox->SetBackgroundColour(*wxRED);
			dndFieldListBox->Enable(false);
			dndFieldListBox->Refresh();
		}
	}

	void ComPanel::onConnectButtonClick(wxCommandEvent & event) {
		if (comComboBox->GetValue().empty()) {
			wxLogMessage("�� ����� COM-����");
			return;
		}

		if (speedComboBox->GetValue().empty()) {
			wxLogMessage("�� ������ ������� ��������");
			return;
		}
		
		optionBoxSizer->Show(false);
		sendFileBoxSizer->Show(true);
		SetSizerAndFit(sendFileBoxSizer, false);
		this->GetParent()->Layout();

		std::string portName = comComboBox->GetValue();
		std::string speed = speedComboBox->GetValue();

		manager = new Manager(portName, std::stoi(speed), this);
		wxLogMessage("��������� ����������� � ����� %s �� �������� %s ���", portName, speed);
	}

	void ComPanel::onDisconnectButtonClick(wxCommandEvent & event) {
		sendFileBoxSizer->Show(false);
		optionBoxSizer->Show(true);
		SetSizerAndFit(optionBoxSizer, false);
		this->GetParent()->Layout();

		delete manager;

		wxLogMessage("��������� ���������� �� �����");
	}

	void ComPanel::initOptionBoxSizer() {
		wxFlexGridSizer * optionFlexGridSizer = new wxFlexGridSizer(3, 2, 5, 25);

		wxStaticText * headerStaticText = new wxStaticText(this, wxID_ANY,
			wxT("�������� ����, �������� � ������� ������������"));
		wxStaticText * comStaticText = new wxStaticText(this, wxID_ANY, wxT("����"));
		wxStaticText * speedStaticText = new wxStaticText(this, wxID_ANY, wxT("��������"));

		std::vector<std::string> ports;
		SerialPort::getPortNames(ports);

		wxString * coms = new wxString[ports.size()];
		for (int i = 0; i < ports.size(); i++) {
			coms[i] = ports[i];
		}

		wxString speeds[] = { "115200", "57600", "56000", "38400", "19200", "14400", "9600" };

		comComboBox = new wxComboBox(this, wxID_ANY, wxEmptyString,
			wxDefaultPosition, wxDefaultSize, ports.size(), coms);
		speedComboBox = new wxComboBox(this, wxID_ANY, wxEmptyString,
			wxDefaultPosition, wxDefaultSize, 7, speeds);

		optionFlexGridSizer->Add(comStaticText, wxSizerFlags().Border().Expand());
		optionFlexGridSizer->Add(comComboBox, wxSizerFlags().Border().Expand());
		optionFlexGridSizer->Add(speedStaticText, wxSizerFlags().Border().Expand());
		optionFlexGridSizer->Add(speedComboBox, wxSizerFlags().Border().Expand());

		optionFlexGridSizer->AddGrowableRow(2, 1);
		optionFlexGridSizer->AddGrowableCol(1, 1);

		wxButton * connectButton = new wxButton(this, ID_CONNECT_BUTTON, wxT("������������"));

		optionBoxSizer = new wxBoxSizer(wxVERTICAL);
		optionBoxSizer->Add(headerStaticText, wxSizerFlags().Border().Expand());
		optionBoxSizer->Add(optionFlexGridSizer, wxSizerFlags().Border().Expand());
		optionBoxSizer->Add(connectButton, wxSizerFlags().Border().Expand());
	}

	void ComPanel::initSendFileBoxSizer() {
		wxButton * disconnectButton = new wxButton(this, ID_DISCONNECT_BUTTON, wxT("�����������"));
		wxStaticText * dndFieldStaticText = new wxStaticText(this, wxID_ANY,
			wxT("���������� ���� ��� �������� � ����������� ����"));
		
		dndFieldListBox = new wxListBox(this, wxID_ANY);
		dndFieldListBox->SetDropTarget(new DnDFile(dndFieldListBox, this));

		sendFileBoxSizer = new wxBoxSizer(wxVERTICAL);
		sendFileBoxSizer->Add(dndFieldStaticText, 0, wxEXPAND | wxALL, 10);
		sendFileBoxSizer->Add(dndFieldListBox, 1, wxEXPAND | wxALL, 10);
		sendFileBoxSizer->Add(disconnectButton, 0, wxEXPAND | wxALL, 10);
	}

	void ComPanel::dndHandler(wxString fileName) {
		std::string path = fileName;
		manager->sendFile(path);
	}

	std::string ComPanel::getSavePath(std::string fileName) {
		wxFileName fullFileName(fileName);
		wxString ext;
		ext.Printf("%s ���� (*.%s)|*.%s", fullFileName.GetExt(), fullFileName.GetExt(), fullFileName.GetExt());
		wxFileDialog saveFileDialog(NULL, wxT("�������� ����� ���������� �����"),
			wxEmptyString, fullFileName.GetName(), ext, wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
		if (saveFileDialog.ShowModal() == wxID_CANCEL) {
			// ����� �������� �� �����������
		}
		return saveFileDialog.GetPath();
	}

	void ComPanel::showProgress(std::string message) {
		progressDialog = new wxProgressDialog(message,
			wxT("�������������..."),
			100,
			NULL,
			wxPD_APP_MODAL |
			wxPD_AUTO_HIDE);/* |wxPD_CAN_ABORT*/
	}

	void ComPanel::updateProgress(int percent) {
		if (progressDialog == NULL) {
			return;
		}
		bool notCanceled = progressDialog->Update(percent, "��������� " + std::to_string(percent) + " %");
		if (!notCanceled) {
			finishProgress();
			delete manager;
			std::string portName = comComboBox->GetValue();
			std::string speed = speedComboBox->GetValue();
			manager = new Manager(portName, std::stoi(speed), this);
		}
	}

	void ComPanel::finishProgress() {
		delete progressDialog;
		progressDialog = NULL;
	}

	void ComPanel::log(std::string message) {
		wxString str(message);
		wxLogMessage(str);
	}
}