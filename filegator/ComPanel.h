#define WIN32_LEAN_AND_MEAN
#include <wx/wxprec.h>
#include <wx/progdlg.h>
#include <wx/filename.h>
#include "AbstractUI.h"
#include "Manager.h"

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#ifndef COMPANEL_H_
#define COMPANEL_H_

namespace filegator {
	const long ID_CONNECT_BUTTON = wxNewId();
	const long ID_DISCONNECT_BUTTON = wxNewId();
	const long ID_RESUME_BUTTON = wxNewId();

	class ComPanel : public wxPanel, public AbstractUI {
		wxBoxSizer * optionBoxSizer;
		wxBoxSizer * sendFileBoxSizer;

		wxComboBox * comComboBox;
		wxComboBox * speedComboBox;
		wxComboBox * byteSizeComboBox;
		wxComboBox * parityComboBox;
		wxComboBox * stopBitsComboBox;

		wxListBox * dndFieldListBox;
		Manager * manager;
		wxProgressDialog * progressDialog;

		void onConnectButtonClick(wxCommandEvent & event);
		void onDisconnectButtonClick(wxCommandEvent & event);
		void initOptionBoxSizer();
		void initSendFileBoxSizer();
		wxDECLARE_EVENT_TABLE();
	public:
		ComPanel(wxPanel * panel);
		void dndHandler(wxString fileName);
		virtual std::string getSavePath(std::string fileName);
		virtual void showProgress(std::string message);
		virtual void updateProgress(int percent);
		virtual void finishProgress();
		virtual void updateConnectionStatus(bool connection);
		virtual void log(std::string message);
	};
}

#endif // COMPANEL_H_