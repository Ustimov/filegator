#include "FileBuilder.h"

namespace filegator {
	FileBuilder::FileBuilder(std::string fileName, int fileSize) {
		this->fileName = fileName;
		this->fileSize = fileSize;
		recievedBytes = 0;
	}

	FileBuilder::~FileBuilder() {
		AbstractFrame * frame;
		while (!frameQueue.empty()) {
			frame = frameQueue.front();
			frameQueue.pop();
			delete frame;
		}
	}

	void FileBuilder::write(std::string fileName) {
		std::fstream outFileStream(fileName, std::ios::out | std::ios::binary);
		AbstractFrame * frame;

		int len;
		while (!frameQueue.empty()) {
			frame = frameQueue.front();
			frameQueue.pop();
			const byte * data = ((InfoFrame*)frame)->getData(len);
			if (data[len - 1] == START_STOP_BYTE && frameQueue.empty()) {
				outFileStream.write((char *)data, len - 1); // �� ����� �������� ����
			}
			else {
				outFileStream.write((char*)data, len);
			}
			delete frame;
		}
		outFileStream.close();
	}

	void FileBuilder::add(AbstractFrame * frame) {
		frameQueue.push(frame);

		int len;
		const byte * data = ((InfoFrame*)frame)->getData(len);

		if (data[len - 1] == START_STOP_BYTE && frameQueue.empty()) {
			len -= 1;
		}
		recievedBytes += len;
	}

	std::string FileBuilder::getFileName() {
		return fileName;
	}

	int FileBuilder::getFrameCount() {
		return frameQueue.size();
	}

	int FileBuilder::getPercent() {
		return 100 * recievedBytes / (double)fileSize;
	}
}