#include "ComPanel.h"
#include <wx/wxprec.h>
#include <wx/dirctrl.h>
#include <wx/dnd.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#ifndef MAINPANEL_H_
#define MAINPANEL_H_

namespace filegator {
	const long ID_BOOK = wxNewId();

	class TabPanel : wxPanel {
		wxGenericDirCtrl * genericDirCtrl;
		wxLog * log;
		wxTextCtrl * logTextCtrl;
		wxBoxSizer * boxSizer;
		void onBeginDrag(wxTreeEvent & event);
		void LogDragResult(wxDragResult result);
	public:
		TabPanel(wxFrame * frame, int x, int y, int w, int h);
	};
}

#endif // MAINPANEL_H_