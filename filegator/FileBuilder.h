#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <fstream>
#include <queue>
#include "Frame.h"

#ifndef FILEBUILDER_H_
#define FILEBUILDER_H_

namespace filegator {
	class FileBuilder {
	public:
		FileBuilder(std::string fileName, int fileSize);
		~FileBuilder();
		std::string getFileName();
		void add(AbstractFrame * frame);
		void write(std::string fileName);
		int getFrameCount();
		int getPercent();
	private:
		std::queue<AbstractFrame *> frameQueue;
		std::string fileName;
		int fileSize;
		int recievedBytes;
	};
}

#endif // FILEBUILDER_H_