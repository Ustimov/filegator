#include "TabPanel.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#ifndef MAINFRAME_H_
#define MAINFRAME_H_

namespace filegator {
	const long ID_OPEN = wxNewId();
	const long ID_ERROR = wxNewId();

	class MainFrame : public wxFrame {
		//void onOpen(wxCommandEvent & event);
		//void onError(wxCommandEvent & event);
		void onExit(wxCommandEvent & event);
		void onAbout(wxCommandEvent & event);
		//void onSize(wxSizeEvent & event);

		TabPanel * tabPanel;

		wxDECLARE_EVENT_TABLE();
	public:
		MainFrame(const wxString & title, const wxPoint & pos, const wxSize & size);
	};
}

#endif // MAINFRAME_H_