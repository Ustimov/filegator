#include <boost/thread.hpp>
#include <queue>
#include "Frame.h"

#ifndef FRAMEFACTORY_H_
#define FRAMEFACTORY_H_

namespace filegator {
	class FrameFactory {
	public:
		FrameFactory(std::string fileName);
		~FrameFactory();
		AbstractFrame * getFrame(int index);
		void delivered(int index);
		int getFrameCount();
		int getPercent();
	private:
		std::vector<AbstractFrame *> frames;
		int fileSize;
		int deliveredBytes;
	};
}

#endif