#include <string>

#ifndef FRAME_H_
#define FRAME_H_

namespace filegator {
	typedef unsigned char byte;

	const int HEADER_LENGTH = 5;
	const byte START_STOP_BYTE = 0xFF;

	class AbstractFrame {
	public:
		enum FrameType {
			INFO = 1, // INFO-����, ���������� ������������ ������
			ACK = 2, // ACK-����, ��� ������������� ���������
			RET = 3, // RET - ���� ��� ������� ���������� ���������� ������������� �����
			INIT = 4, // INIT - ���� ��� ��������� ��������
		};
		virtual const byte * getSerialized(int & len) = 0;
		virtual int getNumber();
		virtual ~AbstractFrame();
	protected:
		byte * data;		
		int frameLength;
		int dataLength;
		int number;
		byte numberArray[2];
		void intToBytes(byte * out, int len, int value);
		int bytesToInt(byte * data, int len);
	};

	class InfoFrame : public AbstractFrame {
	public:
		InfoFrame(byte * data, int len, int number);
		InfoFrame(byte * data, int len, byte * numberArray);
		virtual const byte * getSerialized(int & len);
		const byte * getData(int & len);
		void encodeData();
		bool decodeData();
	private:
		InfoFrame(const InfoFrame &);
		InfoFrame & operator=(const InfoFrame &);
		void init(byte * data, int len);
	};

	class InitFrame : public AbstractFrame {
	public:
		InitFrame(byte * data, int len);
		InitFrame(std::string fileName, int fileSize);
		int getFileSize();
		std::string getFileName();
		virtual const byte * getSerialized(int & len);
	private:
		InitFrame(const InitFrame &);
		InitFrame & operator=(const InitFrame &);
		std::string bytesToString(byte * data, int len);
		std::string fileName;
		int fileSize;
	};

	class BaseNoificationFrame : public AbstractFrame {
	public:
		BaseNoificationFrame(int number);
		BaseNoificationFrame(byte * data);
		virtual const byte * getSerialized(int & len);
	};

	class AckFrame : public BaseNoificationFrame {
	public:
		AckFrame(int number);
		AckFrame(byte * data);
	private:
		AckFrame(const AckFrame &);
		AckFrame & operator=(const AckFrame &);
	};

	class RetFrame : public BaseNoificationFrame {
	public:
		RetFrame(int number);
		RetFrame(byte * data);
	private:
		RetFrame(const RetFrame &);
		RetFrame & operator=(const RetFrame &);
	};
}

#endif // FRAME_H_