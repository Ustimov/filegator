#define WIN32_LEAN_AND_MEAN
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "SerialPort.h"
#include <TCHAR.h>

namespace filegator {
	SerialPort::SerialPort(std::string portName, int baudRate) {
		this->portName = portName;
		this->baudRate = baudRate;

		ioService = new boost::asio::io_service;
		serialPort = new boost::asio::serial_port(*ioService);
		if (!serialPort->is_open()) {
			serialPort->open(portName);
			if (serialPort->is_open()) {
				serialPort->set_option(boost::asio::serial_port_base::baud_rate(baudRate));
				serialPort->set_option(boost::asio::serial_port_base::character_size(8));
				serialPort->set_option(boost::asio::serial_port_base::flow_control(
					boost::asio::serial_port_base::flow_control::hardware));
				serialPort->set_option(boost::asio::serial_port_base::parity(
					boost::asio::serial_port_base::parity::none));
				serialPort->set_option(boost::asio::serial_port_base::stop_bits(
					boost::asio::serial_port_base::stop_bits::one));
			}
		}
	}

	SerialPort::~SerialPort() {
		if (serialPort->is_open()) {
			ioService->stop();
			serialPort->close();
			boost::this_thread::sleep(boost::posix_time::seconds(1));
			delete serialPort;
			delete ioService;
		}
	}

	void SerialPort::run() {
		ioService->run();
	}

	void handler(const boost::system::error_code& ec, size_t bytes_transferred) {
		// ���������� �������� ������
	}

	void SerialPort::write(const byte * data, int len) {
		// boost::asio::write(*serialPort, boost::asio::buffer(data, len));
		boost::asio::async_write(*serialPort, boost::asio::buffer(data, len),
			boost::bind(handler,
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred));
	}

	
	void SerialPort::getPortNames(std::vector<std::string> & ports) {
		const int devicesRange = 65535;
		TCHAR allDevices[devicesRange];

		int countDevices = QueryDosDevice(NULL, (LPWSTR)allDevices, devicesRange);
		if (countDevices) {
			int i = 0;
			for (;;) {
				TCHAR *currentDevice = &allDevices[i];
				int len = _tcslen((const wchar_t *)currentDevice);
				if (len > 3 && len < 7 && _tcsnicmp((const wchar_t *)currentDevice, _T("COM"), 3) == 0) {
					std::wstring wCurrentDevice((const wchar_t *)currentDevice);
					std::string sCurrentDevice(wCurrentDevice.begin(), wCurrentDevice.end());
					ports.push_back(sCurrentDevice);
				}
				while (allDevices[i] != _T('\0')) i++;
				i++;
				if (allDevices[i] == _T('\0')) break;
			}
		}
	}

	bool SerialPort::isCD() {
		DWORD dwModemStatus;
		if (!GetCommModemStatus(serialPort->native_handle(), &dwModemStatus)) {
			return 0;
		}
		return MS_RLSD_ON & dwModemStatus;
	}
}