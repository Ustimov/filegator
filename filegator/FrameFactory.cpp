#include <fstream>
#include "FrameFactory.h"

namespace filegator {
	typedef unsigned char byte;

	FrameFactory::FrameFactory(std::string fileName) {
		deliveredBytes = 0;
		std::ifstream inFileStream(fileName, std::ios::binary | std::ios::in);
		
		// �������� ������ �����
		std::streampos begin, end;
		begin = inFileStream.tellg();
		inFileStream.seekg(0, std::ios::end);
		end = inFileStream.tellg();
		inFileStream.seekg(0, std::ios::beg);
		fileSize = end - begin;

		frames.push_back(new InitFrame(fileName, fileSize));

		const int len = 100;
		byte buf[len];
		int restFileSize = fileSize;
		
		for (int i = 1; restFileSize != 0; i++) {
			inFileStream.read((char *)buf, len);
			if (restFileSize < len) {
				frames.push_back(new InfoFrame(buf, restFileSize, i));
				restFileSize = 0;
			}
			else {
				frames.push_back(new InfoFrame(buf, len, i));
				restFileSize -= len;
			}
		}
	}

	FrameFactory::~FrameFactory() {
		AbstractFrame * frame;
		while (!frames.empty()) {
			frame = frames.back();
			delete frame;
			frames.pop_back();
		}
	}

	void FrameFactory::delivered(int index) {
		InfoFrame * infoFrame = (InfoFrame*)frames[index];
		// ���� ���� ��� ���������, �� ����� ���� �� ��� �����������
		//infoFrame->decodeData();
		int len;
		infoFrame->getData(len);
		deliveredBytes += len;
	}

	int FrameFactory::getPercent() {
		return 100 * deliveredBytes / (double)fileSize;
	}

	AbstractFrame * FrameFactory::getFrame(int index) {
		return frames[index];
	}

	int FrameFactory::getFrameCount() {
		return frames.size();
	}
}