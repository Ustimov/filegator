#ifndef HAMMINGCODE_H_
#define HAMMINGCODE_H_

/*
i - info, c - check
byte:
76543210
0iiicicc
0 = 2^4^6
1 = 2^5^6
3 = 4^5^6
*/

class HammingCode {
public:
	static void encode(unsigned char byte, unsigned char& left, unsigned char& right);
	static bool decode(unsigned char& byte, unsigned char left, unsigned char right);
	static bool checkForError(unsigned char& byte);
	static void byteToArray(unsigned char byte, bool *array);
	static void arrayToByte(bool *array, unsigned char& byte);
};

#endif