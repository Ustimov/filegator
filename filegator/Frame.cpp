#include "Frame.h"
#include "HammingCode.h"

namespace filegator {
	// -- ABSTRACT FRAME ---
	
	int AbstractFrame::bytesToInt(byte * data, int len) {
		const int bit = 8; // ��� � �����
		int fileSize = 0;
		for (int i = 0; i < len; i++) {
			fileSize = fileSize | data[i] << i * bit;
		}
		return fileSize;
	}

	void AbstractFrame::intToBytes(byte * out, int len, int value) {
		const int bit = 8;
		for (int i = 0; i < len; i++) {
			out[i] = out[i] | value >> i * bit;
		}
	}

	int AbstractFrame::getNumber() {
		return number;
	}

	AbstractFrame::~AbstractFrame() {
		delete[] data;
	}

	// --- INFO FRAME ---

	InfoFrame::InfoFrame(byte * data, int len, byte * numberArray) {
		memcpy(this->numberArray, numberArray, 2); // 2 - ���������� ���� ������
		number = bytesToInt(numberArray, 2);
		init(data, len);
	}

	InfoFrame::InfoFrame(byte * data, int len, int number) {
		this->number = number;
		intToBytes(numberArray, 2, number);
		init(data, len);
	}

	void InfoFrame::init(byte * data, int len) {
		dataLength = len;
		frameLength = HEADER_LENGTH + len + 1; // 1 - ����������� �������� ����
		this->data = new byte[dataLength];
		memcpy(this->data, data, dataLength);
	}

	const byte * InfoFrame::getSerialized(int & len) {
		len = frameLength;
		byte * serializedFrame = new byte[frameLength];
		serializedFrame[0] = START_STOP_BYTE;
		serializedFrame[1] = AbstractFrame::INFO;
		serializedFrame[2] = dataLength;
		memcpy(serializedFrame + 3, numberArray, 2);
		memcpy(serializedFrame + HEADER_LENGTH, data, frameLength - HEADER_LENGTH);
		serializedFrame[frameLength - 1] = START_STOP_BYTE;

		return serializedFrame;
	}

	const byte * InfoFrame::getData(int & len) {
		len = dataLength;
		return data;
	}

	bool InfoFrame::decodeData() {
		/*
		unsigned char * newData = new unsigned char[dataLength / 2];
		unsigned char rezByte = 0;
		for (int i = 0, j = 0; i < dataLength / 2; i++, j += 2) {
			if (HammingCode::decode(rezByte, this->data[j], this->data[j + 1]))
				newData[i] = rezByte;
			else return false;
		}

		unsigned char * temp = this->data;
		data = newData;
		delete temp;

		dataLength /= 2;
		frameLength -= dataLength;
		*/
		return true;
	}

	void InfoFrame::encodeData() {
		/*
		unsigned char * newData = new unsigned char[dataLength * 2];
		unsigned char leftByte = 0, rightByte = 0;
		for (int i = 0, j = 0; i < dataLength; i++, j += 2) {
			HammingCode::encode(this->data[i], leftByte, rightByte);
			newData[j] = leftByte;
			newData[j + 1] = rightByte;
		}

		unsigned char * temp = this->data;
		data = newData;
		delete temp;

		frameLength += dataLength;
		dataLength += dataLength;
		*/
	}

	// --- INIT FRAME ---

	InitFrame::InitFrame(std::string fileName, int fileSize) {
		number = 0; // � INIT ����� ����� 0 �� ���������

		this->fileName = fileName;
		this->fileSize = fileSize;

		const int size = 4;
		byte fileSizeBytes[size] = { 0 };
		intToBytes(fileSizeBytes, size, fileSize);

		dataLength = HEADER_LENGTH + size + fileName.size() + 1; // 1 - ��� ��������� �����

		data = new byte[dataLength];
		data[0] = START_STOP_BYTE;
		data[1] = AbstractFrame::INIT;
		data[2] = dataLength - HEADER_LENGTH - 1; // ����� �������������� �����
		data[3] = 0;
		data[4] = 0;
		memcpy(data + 5, fileSizeBytes, size);
		memcpy(data + 5 + size, fileName.c_str(), fileName.size());
		data[dataLength - 1] = data[0] = START_STOP_BYTE;
	}

	InitFrame::InitFrame(byte * data, int len) {
		number = 0; // � INIT ����� ����� 0 �� ���������
		const int size = 4; // ���������� ���� �������
		fileSize = bytesToInt(data, size);
		fileName = bytesToString(data + size, len - size);
	}

	std::string InitFrame::bytesToString(byte * data, int len) {
		std::string fileName;
		for (int i = 0; i < len; i++) {
			fileName.push_back(data[i]);
		}
		return fileName;
	}

	const byte * InitFrame::getSerialized(int & len) {
		len = dataLength;
		return data;
	}

	std::string InitFrame::getFileName() {
		return fileName;
	}

	int InitFrame::getFileSize() {
		return fileSize;
	}

	// --- BASE NOTIFICATION FRAME ---

	BaseNoificationFrame::BaseNoificationFrame(int number) {
		frameLength = HEADER_LENGTH;
		this->number = number;
		const int size = 2;
		byte n[size] = { 0 };
		intToBytes(n, size, number);
		memcpy(numberArray, n, size);
	}

	BaseNoificationFrame::BaseNoificationFrame(byte * data) {
		// ��������������, ��� data - �������� ������ ����� HEADER_LENGTH
		frameLength = HEADER_LENGTH;
		number = bytesToInt(data + 2, 2);
		memcpy(numberArray, data + 2, 2);
	}

	const byte * BaseNoificationFrame::getSerialized(int & len) {
		len = frameLength;
		return data;
	}

	// --- ACK FRAME ---

	AckFrame::AckFrame(int number) : BaseNoificationFrame(number) {
		data = new byte[HEADER_LENGTH] { START_STOP_BYTE, AbstractFrame::ACK,
			numberArray[0], numberArray[1], START_STOP_BYTE };
	}

	AckFrame::AckFrame(byte * data) : BaseNoificationFrame(data) {
		this->data = new byte[HEADER_LENGTH] { START_STOP_BYTE, AbstractFrame::ACK,
			numberArray[0], numberArray[1], START_STOP_BYTE };
	}

	// --- RET FRAME ---

	RetFrame::RetFrame(int number) : BaseNoificationFrame(number) {
		data = new byte[HEADER_LENGTH] { START_STOP_BYTE, AbstractFrame::RET,
			numberArray[0], numberArray[1], START_STOP_BYTE };
	}

	RetFrame::RetFrame(byte * data) : BaseNoificationFrame(data) {
		this->data = new byte[HEADER_LENGTH] { START_STOP_BYTE, AbstractFrame::RET,
			numberArray[0], numberArray[1], START_STOP_BYTE };
	}

	//------------------------------------------------------------------------

	/*
		//������� ������. ���� ������ �� 50 ���������
		if (FrameFactory::getPercent() > 50 && FrameFactory::spoilFlag) {
			serializedFrame[headerLength + 1]++;
			FrameFactory::spoilFlag = 0;
		}
		
		
		//������ ��� ��� ������������ � ��������� �������.
		//if (FrameFactory::spoilFlag) {
		//	serializedFrame[headerLength + 1]++;
		//	FrameFactory::spoilFlag = 0;
		//}
		//else 
		//	FrameFactory::spoilFlag = 1;	
	*/
}
