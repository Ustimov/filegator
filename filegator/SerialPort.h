#define WIN32_LEAN_AND_MEAN
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <string>
#include <vector>

#ifndef SERIALPORT_H_
#define SERIALPORT_H_

namespace filegator {
	typedef unsigned char byte;
	class SerialPort {
	public:
		SerialPort(std::string portName, int baudRate);
		~SerialPort();
		bool isCD();
		void run();
		void write(const byte * data, int len);
		template <typename F, typename T>
		void read(byte * buf, int len, F handler, T obj);
		static void getPortNames(std::vector<std::string> & ports);
	private:
		boost::asio::io_service * ioService;
		boost::asio::serial_port * serialPort;
		std::string portName;
		int baudRate;
	};

	template <typename F, typename T>
	void SerialPort::read(byte * buf, int len, F handler, T obj) {
		boost::asio::async_read(*serialPort, boost::asio::buffer(buf, len),
			boost::bind(handler, obj,
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred));
	}
}

#endif // SERIALPORT_H_