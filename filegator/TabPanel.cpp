#include "TabPanel.h"

namespace filegator {
	TabPanel::TabPanel(wxFrame * frame, int x, int y, int w, int h)
		: wxPanel(frame, wxID_ANY, wxPoint(x, y), wxSize(w, h)) {
		
		logTextCtrl = new wxTextCtrl(this, wxID_ANY, wxEmptyString,
			wxPoint(0, 250), wxSize(100, 50), wxTE_MULTILINE);
		log = wxLog::SetActiveTarget(new wxLogTextCtrl(logTextCtrl));
	
		ComPanel * panel = new ComPanel(this);
		genericDirCtrl = new wxGenericDirCtrl(this);
		genericDirCtrl->Connect(wxID_ANY, wxEVT_TREE_BEGIN_DRAG,
			wxTreeEventHandler(TabPanel::onBeginDrag), NULL, this);
		
		boxSizer = new wxBoxSizer(wxVERTICAL);
		boxSizer->Add(panel, wxSizerFlags(1).Border().Expand());
		boxSizer->Add(logTextCtrl, wxSizerFlags(1).Border().Expand());

		wxBoxSizer * dirCtrlAndComPanelBoxSizer = new wxBoxSizer(wxHORIZONTAL);
		dirCtrlAndComPanelBoxSizer->Add(genericDirCtrl, wxSizerFlags(1).Border().Expand());
		dirCtrlAndComPanelBoxSizer->Add(boxSizer, wxSizerFlags(1).Border().Expand());

		SetSizerAndFit(dirCtrlAndComPanelBoxSizer);
	}

	void TabPanel::onBeginDrag(wxTreeEvent & WXUNUSED(event)) {
		wxFileDataObject data;
		data.AddFile(genericDirCtrl->GetPath());
		wxDropSource dragSource(this);
		dragSource.SetData(data);
		dragSource.DoDragDrop();
	}
}