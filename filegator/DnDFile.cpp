#include "DndFile.h"

namespace filegator {
	DnDFile::DnDFile(wxListBox * dndField, ComPanel * comPanel) {
		this->dndField = dndField;
		this->comPanel = comPanel;
	}

	bool DnDFile::OnDropFiles(wxCoord, wxCoord, const wxArrayString & filenames) {
		comPanel->dndHandler(filenames[0]);
		return true;
	}
}