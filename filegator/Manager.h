#define WIN32_LEAN_AND_MEAN
#include <boost/thread.hpp>
#include "AbstractUI.h"
#include "SerialPort.h"
#include "FrameFactory.h"
#include "FileBuilder.h"
#include "Frame.h"

#ifndef MANAGER_H_
#define MANAGER_H_

namespace filegator {
	class Manager {
	public:
		Manager(std::string portName, int baudRate, AbstractUI * ui);
		~Manager();
		void sendFrame(AbstractFrame * frame);
		void sendFile(std::string path);
		void checkConnection();
		void resume();
	private:
		SerialPort * serialPort;
		FrameFactory * frameFactory;
		FileBuilder * fileBuilder;
		AbstractUI * ui;
		byte * buf;
		byte numberArrayBuf[2];	
		std::string portName;
		int baudRate;
		bool pauseRun;
		bool stopThreads;
		
		void runThread();
		void listenPort();
		void onReceiveHeader(const boost::system::error_code & ec, size_t bytes_transferred);
		void onReceiveInfoFrame(const boost::system::error_code & ec, size_t bytes_transferred);
		void onReceiveInitFrame(const boost::system::error_code & ec, size_t bytes_transferred);
	};
}

#endif // MANAGER_H_